const express = require('express');
const router = express.Router();
const productControllers = require('../controllers/productControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

router.post('/',verify, verifyAdmin, productControllers.addProducts);

router.get('/',verify, productControllers.getAllProducts);

router.post('/searchByCategory',verify, productControllers.findProductByCategory);

router.put('/updateProduct/:id',verify, verifyAdmin, productControllers.updateProduct);

router.put('/archiveProduct/:id', verify, verifyAdmin, productControllers.archiveProduct);

router.put('/activateProduct/:id', verify, verifyAdmin, productControllers.activateProduct);

router.get('/getAllOrders', verify, verifyAdmin, productControllers.getAllOrders);

router.get('/showActiveProduct', productControllers.findActiveProduct);

router.get('/singleProduct/:id', productControllers.findSingleProduct);



module.exports = router;

