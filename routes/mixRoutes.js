const express = require('express');
const router = express.Router();
const mixControllers = require('../controllers/mixControllers');

router.post('/', mixControllers.addMix);

router.post('/findmix', mixControllers.findMix);

module.exports = router;