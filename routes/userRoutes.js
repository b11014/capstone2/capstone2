const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

router.get('/', verify, verifyAdmin, userControllers.getAllUsers);

router.post('/', userControllers.registerUser);

router.post('/loginUser', userControllers.loginUser);

router.put('/beAdmin/:id', verify, verifyAdmin, userControllers.updateUserAdmin);

router.post('/purchase', verify, userControllers.purchase);

router.get('/userShowOrders', verify, userControllers.getUserOrders);

module.exports = router;