//module
const express = require('express');
// port #
const port = 4000;
//server
const app = express();
//database
const mongoose = require('mongoose');
//cors - allowing to examine the request before it will be made
const cors =  require('cors');

//db - paste srv from mongoDB
mongoose.connect("mongodb+srv://admin:admin@capstone-2.gyhpt.mongodb.net/capstone-2?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
    //can avoid errors while future changes and patches implemented in mongodb
});

//db notification of connections

let db  = mongoose.connection;
    db.on("error", console.error.bind(console, "Connection Error!"));
    // on running
    db.once("open", () => console.log("Successfully connected to MongoDB."));
    // once running

//middlewares - provides other services between connection operating system and application
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//group routing

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

// const mixRoutes = require('./routes/mixRoutes');
// app.use('/mixs', mixRoutes);

//port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));