const jwt = require('jsonwebtoken');
const secret = "Capstone2";

module.exports.createAccessToken = (user) => {
    console.log(user);
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    console.log(data);
    return jwt.sign(data, secret, {})
}

//verify user
    module.exports.verify = (req, res, next) => {
        let token = req.headers.authorization;

        if(typeof token === "undefined"){
            return res.send({auth:"Failed to process. No token."})
        }else{
            console.log(token);

            token = token.slice(7, token.length)
            console.log(token);

            jwt.verify(token, secret, (err, decodedToken) => {
                if(err){
                    res.send({auth: "Failed!", message: err.message})
                }else{
                    console.log(decodedToken);
                    req.user = decodedToken
                    next();
                }
            })

        }
    };

//verify admin
    module.exports.verifyAdmin = (req, res, next) =>{
        if(req.user.isAdmin){
            next();
        }else{
            return res.send({auth: "Failed!", message: "Action Forbidden!"})
        }
    };