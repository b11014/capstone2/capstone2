const mongoose = require('mongoose');

let mixSchema = new mongoose.Schema({
	name: {
		type: String
	},
	desc: {
		type: String
	},
	category: {
		type: String,
		enum: ['Choco', 'Milk', 'Fruity']
	}
})


module.exports = mongoose.model('Mix', mixSchema)