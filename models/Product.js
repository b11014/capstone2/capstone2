const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'First name is required.']
    },
    desc: {
        type: String,
        required: [true, 'Last name is required.']
    },
    category:{
        type: String,
        required: [true, 'Category is required.'],
    },
    price: {
        type: Number,
        required: [true, 'Price is required.']
    },
    isAvailable: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    buyer: [
        {
            userId: {
                type: String,
                required: [true, "Product Id is required."]
            },
            orderDate: {
                type: Date,
                default: new Date()
            }
        }
    ]

});
module.exports = mongoose.model("Product", productSchema);