const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required.']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    password: {
        type: String,
        required: [true, 'Password is required.']
    },
    mobileNumber: {
        type: String,
        required: [true, 'Mobile number is required.']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
            productId: {
                type: String,
                required: [true, "Product Id is required."]
            },
            orderDate: {
                type: Date,
                default: new Date()
            },
            price: {
                type: Number
            }
        }
    ],
    totalAmount: [{
            type: Number,
            default: 0
        }],
    sumOfOrders: Number
});

module.exports = mongoose.model("User", userSchema);