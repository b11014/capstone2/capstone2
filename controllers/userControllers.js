const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcryptjs');

const auth = require('../auth');


// register user

module.exports.registerUser = (req, res) => {

    const hashedPW = bcrypt.hashSync(req.body.password, 10);
    
    let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPW,
        mobileNumber: req.body.mobileNumber
    });
    
    User.findOne({email: req.body.email}).then(foundEmail => {
		if(foundEmail === null){
			return newUser.save(), res.send("Successfully registered.")
		}else{
			return res.send("Email is already registered.")
		}
	})
	.catch(err => res.send(err));
}

//login user

module.exports.loginUser = (req, res) => {
    User.findOne({email: req.body.email}).then(foundUser => {
        if (foundUser === null){
            return res.send("Not yet registered!")
        }else{
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);

            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(foundUser)})
            }else{
                return res.send("Incorrect password. Please try again.")

            } 
        }
    })
    .catch(err => res.send(err));
}



//Show users admin

module.exports.getAllUsers = (req, res) => {
    User.find({}).then(result => res.send(result)).catch(err => res.send(err));
};
//update user to admin

module.exports.updateUserAdmin = (req, res) => {

    let updates = {
        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.id, updates, {new: true}).then(result => res.send(result)).catch(err => res.send(err));
};


//create order


module.exports.purchase = async (req, res) => {

    if(req.user.isAdmin){
        return res.send('Action Forbidden');
    }
 
    let isUserUpdated = await User.findById(req.user.id).then(user => {
        console.log(user);

        let newOrder = {
            productId: req.body.productId,
            price: req.body.price
        }
        
            user.orders.push(newOrder);
            user.totalAmount.push(newOrder.price);

            let sum = user.totalAmount.reduce((prev, current) =>
                prev + current);

        User.findByIdAndUpdate(req.user.id, {sumOfOrders: sum}, {new: true}).then((user) => user).catch((err) => err.message);

            return user.save().then(user => true).catch(err => err.message)
    })

        if(isUserUpdated !== true){
            return res.send({message: isUserUpdated})
        }

        let isProductUpdated = await Product.findById(req.body.productId).then(product => {
            console.log(product)

            let newBuyer = {
                userId: req.user.id
            }
            product.buyer.push(newBuyer)

            return product.save().then(product => true).catch(err => err.message)
        })


        if(isProductUpdated !== true){
            return res.send({message: isProductUpdated})
        }

        if(isUserUpdated && isProductUpdated){
            return res.send({message: 'Added to your cart.'})
        }
};

//getting one user orders

module.exports.getUserOrders = (req, res) => {
    User.findById(req.user.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

