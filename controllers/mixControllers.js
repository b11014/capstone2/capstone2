const Mix = require('../models/Mix');
const mongoose = require('mongoose');

module.exports.addMix = (req, res) => {
	let mix = new Mix ({
		name: req.body.name,
		desc: req.body.desc,
		category: req.body.category
	})
		return mix.save().then(result => res.send(result)).catch(err => res.send(err));

};


module.exports.findMix = (req, res) => {
	Mix.find({category: req.body.category}).then(result => {
		console.log(req.body.category);
		if(req.body.category === null || req.body.category !== Mix.category){
			return res.send("No matched. Try other category")
		}else{
			return res.send(result)
		}
	})
	.catch(err => res.send(err));
};
