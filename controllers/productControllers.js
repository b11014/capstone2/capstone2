const Product = require("../models/Product");

module.exports.addProducts = (req, res) => {
    
    let newProd = new Product ({
        name: req.body.name,
        desc: req.body.desc,
        category: req.body.category,
        price: req.body.price,
        stocks: req.body.stocks
        
    });
    
    Product.findOne({name: req.body.name}).then(foundx => {
        if(foundx === null){
            return newProd.save(),  res.send("Added!");
        }else{
            return res.send("Failed to add!")
        }
    })
    .catch(err => res.send(err));
};

//retrieve all products
module.exports.getAllProducts = (req, res) => {
    Product.find({}).then(result => res.send(result)).catch(err => res.send(err));
};

//retrieve by category
// module.exports.getAllProductsByCategory = (req, res) => {
//     Product.find({category: req.body.category}).then(foundy => {
//         if(foundy === 'undefined'){
//             return res.send("No matched");
//         }else{
//             return res.send(Product)
//         }
//     })
//     .catch(err => res.send(err))
// };

// module.exports.findProduct = (req, res) => {
// 	Product.find({category: req.body.category}).then(result => {
// 		if(req.body.category === null || req.body.category !== Mix.category){
// 			return res.send("No matched.")
// 		}else{
// 			return res.send(result)
// 		}
// 	})
// 		.catch(err => res.send(err));
// };

// module.exports.findProduct = (req, res) => {
//     Product.find({category: req.body.category}).then(result => {
//         console.log(req.body.category);
//         console.log(result.category)
//         if(result === req.body.category){
//             return res.send('No matched')
//         }else{
//             return res.send(result)
//         }
//     })
//     .catch(err => res.send(err));
// };


module.exports.findProductByCategory = (req, res) => {
    Product.find({category: {$regex: req.body.category, $options: '$i'}}).then(result => {
        if(result.length === 0){
            return res.send('No product found. Try different category.')
        }else{
            return res.send(result)
        }
    })
};


module.exports.findActiveProduct = (req, res) => {
    Product.find({isAvailable: true}).then(result => res.send(result)).catch(err => res.send(err));
};


module.exports.findSingleProduct = (req, res) => {
     Product.findOne({ _id: req.params.id }).then(prod=> res.send(prod)).catch(err => res.send(err));
};



//update category
module.exports.updateProduct = (req, res) => {
    let updatedProd = {
        name: req.body.name,
        desc: req.body.desc,
        price: req.body.price,
        category: req.body.category
    };
    Product.findByIdAndUpdate(req.params.id, updatedProd, {new: true}).then(updated => res.send(updated)).catch(err => res.send(err));
}

//archive a product
module.exports.archiveProduct = (req, res) => {
    let archive = {
        isAvailable: false
    }
    Product.findByIdAndUpdate(req.params.id, archive, {new: true}).then(result => res.send(result)).catch(err => res.send(err));
};

module.exports.activateProduct = (req, res) => {
    let archive = {
        isAvailable: true
    }
    Product.findByIdAndUpdate(req.params.id, archive, {new: true}).then(result => res.send(result)).catch(err => res.send(err));
};


// admin only getting user orders

module.exports.getAllOrders = (req, res) => {
  let userOrders = [];
  Product.find({}).then((result) => {
    result.forEach((item) => {
        item.buyer.length !== 0 &&
          userOrders.push({
            name: item.name,
            category: item.category,
            desc: item.desc,
            price: item.price,
            buyer: item.buyer,
          });
      });
      res.send({ userOrders });
    })
    .catch((err) => res.send(err));
};
